clc
%clear all
close all
load 'fil'
load 'odniesieniedox'
load 'odniesieniedoy'
load 'net'
%% Wczytanie danych
fid = fopen('do12501.txt','r');
dataCell = textscan(fid,'%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s');
fclose(fid);
dl=length(dataCell{1,3});
k=1;
%% konwersja danych pomiarowwych
for n=1:dl
   for k=1:4
    punktx(n,k)=int16(hex2dec(dataCell{1,(3+(k-1)*4)}(n,1))+256*(hex2dec(dataCell{1,(3+(k-1)*4)+1}(n,1))));
    punkty(n,k)=int16(hex2dec(dataCell{1,(5+(k-1)*4)}(n,1))+256*(hex2dec(dataCell{1,(5+(k-1)*4)+1}(n,1))));

   end
end
g=1;

for n=1:dl
   for k=1:4
    zbiorczypunktx(g)=punktx(n,k);
    zbiorczypunkty(g)=punkty(n,k);
    g=g+1;
   end
end
sredniax=mean(zbiorczypunktx);
sredniay=mean(zbiorczypunkty);
for k=1:length(zbiorczypunktx)
zbiorczypunktx(k)=zbiorczypunktx(k)-sredniax;
zbiorczypunkty(k)=zbiorczypunkty(k)-sredniay;
g=g+1;
end
%% polozenie
for n=1:dl
polozenie(n)=int16(hex2dec(dataCell{1,19}(n,1))+256*(hex2dec(dataCell{1,20}(n,1))));
end
%% Polozenie na probki
for n=1:dl
polozeniewprobkach(n)=polozenie(n)*2.4896*4;
end
%% FFT
% 
Fs = 250;           % Sampling frequency
t = 0:1/Fs:1/Fs*2000;  % Time vector
L = length(t);      % Signal length
for n=1:2000
    X(n)=double(zbiorczypunktx(n+2.1e04));
end

for n=1:length(zbiorczypunktx)
    zbiorczypunktxfloat(n)=double(zbiorczypunktx(n));
end

for n=1:length(zbiorczypunktx)
    zbiorczypunktyfloat(n)=double(zbiorczypunkty(n));
end
n = 2^nextpow2(L);

Y = fft(X,n);
f = Fs*(0:(n/2))/n;
P = abs(Y/n);

figure(50)
plot(f,P(1:n/2+1),'LineWidth',2)
grid on
title('Gaussian Pulse in Frequency Domain kanalx')
xlabel('Frequency (f)')
ylabel('|P(f)|')
% 
% 
Fs = 250;           % Sampling frequency
t = 0:1/Fs:1/Fs*5000;  % Time vector
L = length(t);      % Signal length
for n=1:5000
    X(n)=double(zbiorczypunktx(n+2.35e04));
end
n = 2^nextpow2(L);

Y = fft(X,n);
f1 = Fs*(0:(n/2))/n;
P1 = abs(Y/n);

figure(51)
plot(f1,P1(1:n/2+1),'LineWidth',2)
grid on
title('Gaussian Pulse in Frequency Domain ')
xlabel('Frequency (f)')
ylabel('|P(f)|')
% calkadobra=sum(f1(1:))
% calkawada=
% 
% 
% 
% Fs = 250;           % Sampling frequency
% t = 0:1/Fs:1/Fs*600;  % Time vector
% L = length(t);      % Signal length
% p=0;
% for n=6000:6600
%     p=p+1;
%     X(p)=double(zbiorczypunkty(n));
% end
% n = 2^nextpow2(L);
% 
% Y = fft(X,n);
% f = Fs*(0:(n/2))/n;
% P = abs(Y/n);
% 
% figure(12)
% plot(f,P(1:n/2+1))
% title('Gaussian Pulse in Frequency Domain Kanaly')
% xlabel('Frequency (f)')
% ylabel('|P(f)|')
% 
% 
% Fs = 250;           % Sampling frequency
% t = 0:1/Fs:1/Fs*600;  % Time vector
% L = length(t);      % Signal length
% u=0;
% for n=5400:6000
%     u=u+1;
%     X(u)=double(zbiorczypunkty(n));
% end
% n = 2^nextpow2(L);
% 
% Y = fft(X,n);
% f = Fs*(0:(n/2))/n;
% P = abs(Y/n);
% 
% figure(13)
% plot(f,P(1:n/2+1))
% title('Gaussian Pulse in Frequency Domain Kanaly')
% xlabel('Frequency (f)')
% ylabel('|P(f)|')

%% filtr
filtrx=filter(Hlp,smooth(zbiorczypunktxfloat,3));
filtry=filter(Hlp,smooth(zbiorczypunktyfloat,3));

%% fazor
for n=1:length(zbiorczypunktx)
    fazor(n)=sqrt(filtrx(n)^2+filtry(n)^2);
end
%% Pochodne
pochodnax=diff(filtrx);
pochodnay=diff(filtry);
%% Vpp
for n=1:length(zbiorczypunktx)-1000
Vppx(n)=max(filtrx(n:(n+1000)))-min(filtrx(n:(n+1000)));
end
for n=1:length(zbiorczypunkty)-1000
Vppy(n)=max(filtry(n:(n+1000)))-min(filtry(n:(n+1000)));
end
%Vppx=Vppx(0.2e04:4.5e04);
%Vppy=Vppy(0.2e04:4.5e04);
sumaVpp=Vppx+Vppy;
%% poziom
%pop
pozwol=0;
ind=1;
for n=1:length(Vppy)
poziom(n)=45;
if(sumaVpp(n)<poziom(n))
    sumaVpp(n)=0;
     pozwol=1;
else
             if(pozwol==1)
            pozwol=0;
            wgore(ind)=n;
            ind=ind+1;
        end  
end
end

for n=1:length(wgore)
    in=0;
    maxpom=0;
    while(and(sumaVpp(wgore(n)+in)>45,wgore(n)+in<length(sumaVpp)))
        
        if(sumaVpp(wgore(n)+in)>maxpom)
        maxp(n)=(wgore(n)+in);
        maxpom=sumaVpp(wgore(n)+in);
        end
        if(sumaVpp(wgore(n)+in)>=maxpom)
        maxk(n)=(wgore(n)+in);
        maxpom=sumaVpp(wgore(n)+in);
        end
        in=in+1;
    end
end

for n=1:length(wgore)
    srodki(n)= (maxp(n)+maxk(n))/2;
end
indeksy=1;
for n=1:length(wgore)
    
if((floor(srodki(n)+2000)>length(sumaVpp)))
    srodki(n)=length(sumaVpp)-2001;
end
if((floor(srodki(n)-2000)<1))
    srodki(n)=2001;
end

    if(sumaVpp(int32(srodki(n)))>=max(sumaVpp((floor(srodki(n)-2000)):(floor(srodki(n)+2000)))))
    srodkiwlasciwe(indeksy)= srodki(n)+500;
    indeksy=indeksy+1;
    end
end
%% wykrycie wad

for n=1:9
   wadax(n,:)=filtrx( srodkiwlasciwe(n)-1500:srodkiwlasciwe(n)+1500) ;
   waday(n,:)=filtry( srodkiwlasciwe(n)-1500:srodkiwlasciwe(n)+1500) ;
   wadax(n,:)=wadax(n,:)-mean(wadax(n,:)) ;
   waday(n,:)=waday(n,:)-mean(waday(n,:)) ;
   odniesieniex=odniesieniedox(n,:);
   odniesieniey=odniesieniedoy(n,:);
   p=polyfit(wadax(n,:),waday(n,:),1);
   wspkier(n)=atan(p(1,1));
   Vppx(n)=max(wadax(n,:))-min(wadax(n,:));
   Vppy(n)=max(waday(n,:))-min(waday(n,:));
   [Y,I]=max((waday(n,:)));
   [Y1,I1]=min((waday(n,:)));
   pom1=abs(I-I1);
   [Y,I]=max((wadax(n,:)));
   [Y1,I1]=min((wadax(n,:)));
   pom2=abs(I-I1);
   wartoscrozx(n)=pom2;
   wartoscrozy(n)=pom1;
   p1=corrcoef( wadax(n,:), waday(n,:));
   kor(n)=p1(1,2);
   p1=corrcoef( wadax(n,:), odniesieniex);
   korporx(n)=p1(1,2);
  p1=corrcoef( waday(n,:), odniesieniey);
   korpory(n)=p1(1,2);
   wspsumaVpp(n)=sumaVpp(ceil(srodkiwlasciwe(n)-500));
   
   d1=diff(wadax(n,:));
   p1=corrcoef( d1,diff( odniesieniex));
   korporxdiff(n)=p1(1,2);
   Vppxdiff(n)=max(d1)-min(d1);
 
   d1=diff(waday(n,:));
   p1=corrcoef( d1,diff( odniesieniey));
   korporydiff(n)=p1(1,2);
  
   Vppydiff(n)=max(d1)-min(d1);
   figure(n+100)
   plot((1:length(wadax(n,:)))/50,wadax(n,:),(1:length(wadax(n,:)))/50,waday(n,:),'LineWidth',2)
%    hold on
%    plot(waday(n,:),'r');
   grid on
title('Zarejestrowana wada')
xlabel('Przemieszczenie[mm]')
ylabel('Amplituda [mV]')
legend('Re','ImZ')
   figure(n+200)
   plot(wadax(n,:),waday(n,:),'*')
grid on
title('Zarejestrowana wada')
xlabel('ReZ')
ylabel('ImZ')
%legend('Re','ImZ')
   Fs = 250;           % Sampling frequency
t = 0:1/Fs:1/Fs*3000;  % Time vector
L = length(t);      % Signal length

    X=wadax(n,:);

n1 = 2^nextpow2(L);

Y = fft(X,n1);
f1 = Fs*(0:(n1/2))/n;
P1 = abs(Y/n1);


   Fs = 250;           % Sampling frequency
t = 0:1/Fs:1/Fs*3000;  % Time vector
L = length(t);      % Signal length

    X=waday(n,:);

n1 = 2^nextpow2(L);

Y = fft(X,n1);
f1 = Fs*(0:(n1/2))/n;
P2 = abs(Y/n1);
   p2=corrcoef( P1, P2);
korf(n)=p2(1,2);


% figure(300+n)
% plot(f1,P1(1:n1/2+1))
% title('Gaussian Pulse in Frequency Domain Kanaly')
% xlabel('Frequency (f)')
% ylabel('|P(f)|')
   
   

end
milimetry=1:length(filtrx);
milimetry=milimetry*1250/50000;
p=polyfit(wadax(6,:),waday(6,:),1)
x=-2000:1:2000;
figure(300)
plot(wadax(6,:),waday(6,:),'*')
hold on
plot(x,p(1,1)*x+p(1,2),'r','LineWidth',3)
grid on
title('Zarejestrowana wada')
xlabel('ReZ')
ylabel('ImZ')
%% wskazniki dla wykrytych wad
%grubosc
for n=1:length(srodkiwlasciwe)
wyjscie(n,1)=n;
end
%% wieksza wartosc

for n=1:length(zbiorczypunktx)
    if(filtrx(n)>filtry(n))
        wieksza(n)=filtrx(n);

    else
       wieksza(n)=filtry(n); 
      
    end
end

%% Wykresy

figure(1)
plot(zbiorczypunktx)
title('KanalX')
figure(2)
plot(zbiorczypunkty)
title('KanalY')
figure(3)
plot(polozenie)
title('Polozenie')
figure(4)
plot(polozenie,polozeniewprobkach)
title('Probki')
figure(5)
plot(pochodnax)

title('Probki')
figure(6)
plot(zbiorczypunktx)
title('Probki')
grid on
hold on
plot(filtrx,'r')
title('Probki')
figure(7)
plot(milimetry,filtrx*3.3/4.0960-mean(filtrx(1000:end-2000)*3.3/4.0960)+120,milimetry,filtry*3.3/4.0960-mean(filtry(1000:end-2000)*3.3/4.0960),'LineWidth',2)
grid on
title('Wykresy czasowe')
xlabel('Polozenie [mm]')
ylabel('Amplituda [mV]')
legend('Re','ImZ')
figure(5)
plot(milimetry(1:end-1),pochodnax,'LineWidth',2)
grid on
title('Wykres pochodnej dla ReZ')
xlabel('Polozenie [mm]')
ylabel('Wartosc pochodnej')

figure(8)
plot((abs(filtry)+abs(filtrx)))
title('Probki suma')
% figure(9)
% plot((abs(pochodnay.^2)+abs(pochodnax.^2)).^2)
% title('Probki iloczyn')
% 
figure(10)
plot(fazor)
figure(11)
plot(wieksza)
figure(12)
plot(((filtry)+(filtrx)))
title('Probki suma')
figure(13)
plot(Vppx)
title('Probki suma')
figure(14)
plot(Vppy)
title('Probki suma')
figure(15)
plot(milimetry(1:length(sumaVpp)),sumaVpp*3.3/4.0960)
grid on
title('Suma wartości międzyszczytowych dla ReZ i ImZ')
xlabel('Polozenie [mm]')
ylabel('Amplituda [mV]')
%hold on 
%plot(poziom,'r')
















nowewyjscie=[wyjscie]; 
nowewspkier=[wspkier ]; 
nowewartoscrozx=[wartoscrozx ];
nowewartoscrozy=[wartoscrozy ];
nowekor=[kor ];
nowekorporx=[korporx ];
nowekorpory=[korpory ];
nowekorf=[korf ];
nowekorporxdiff=[korporxdiff ];
nowekorporydiff=[korporydiff ];
noweVppxdiff=[Vppxdiff ];
noweVppydiff=[Vppydiff ];

for n=1:9
    wejsciesiec(n,:)=[nowewspkier(n) nowewartoscrozx(n) nowewartoscrozy(n) nowekor(n)  nowekorporx(n) nowekorpory(n)  nowekorf(n)  nowekorporxdiff(n)  nowekorporydiff(n)  noweVppxdiff(n) noweVppydiff(n)  wspsumaVpp(n)];
    wada=round(mean(sim(net,wejsciesiec(n,:)')))
%     wada1=round(mean(sim(net1,wejsciesiec(n,:)')))
%     wada2=round(mean(sim(net2,wejsciesiec(n,:)')))
end















