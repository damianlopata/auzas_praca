/**************************************************************
WinFilter version 0.8
http://www.winfilter.20m.com
akundert@hotmail.com

Filter type: Band Pass
Filter model: Butterworth
Filter order: 2
Sampling Frequency: 62 MHz
Fc1 and Fc2 Frequencies: 0.050000 MHz and 1.000000 MHz
Coefficents Quantization: float

Z domain Zeros
z = -1.000000 + j 0.000000
z = -1.000000 + j 0.000000
z = 1.000000 + j 0.000000
z = 1.000000 + j 0.000000

Z domain Poles
z = 0.935589 + j -0.067063
z = 0.935589 + j 0.067063
z = 0.996484 + j -0.003910
z = 0.996484 + j 0.003910
***************************************************************/
#define NCoef 4
float iir(float NewSample) {
    float ACoef[NCoef+1] = {
        0.00230848000258356780,
        0.00000000000000000000,
        -0.00461696000516713560,
        0.00000000000000000000,
        0.00230848000258356780
    };

    float BCoef[NCoef+1] = {
        1.00000000000000000000,
        -3.86414486047465780000,
        5.60201440314938020000,
        -3.61152985863446880000,
        0.87366055497940509000
    };

    static float y[NCoef+1]; //output samples
    static float x[NCoef+1]; //input samples
    int n;

    //shift the old samples
    for(n=NCoef; n>0; n--) {
       x[n] = x[n-1];
       y[n] = y[n-1];
    }

    //Calculate the new output
    x[0] = NewSample;
    y[0] = ACoef[0] * x[0];
    for(n=1; n<=NCoef; n++)
        y[0] += ACoef[n] * x[n] - BCoef[n] * y[n];
    
    return y[0];
}
